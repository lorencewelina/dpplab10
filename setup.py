# setup.py
from distutils.core import setup

setup(name='lib',
	version='1.0',
	author='Ewelina Lorenc',
	author_email='ewelinalorenc@vp.pl',
	url='htttp://www.lelewina.com',
	packages=['lib', 'lib.printer'],
	install_requires=['terminaltables']
)
