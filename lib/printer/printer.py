from terminaltables import AsciiTable
import lib

def print_event_occurrences(event_occurrences):
    table_data = [['ID', 'Nazwa', 'Opis', 'Data', 'Powtarzalnosc', 'Priorytet']]
    for event_occurrence in event_occurrences:
        e = event_occurrence.parent
        if e.periodtype == lib.PeriodType.DAILY:
            periodType = "Dzienna"
        elif e.periodtype == lib.PeriodType.WEEKLY:
            periodType = "Tygodniowa"
        elif e.periodtype == lib.PeriodType.MONTHLY:
            periodType = "Miesieczna"
        elif e.periodtype == lib.PeriodType.YEARLY:
            periodType = "Roczna"
        if e.prioritytype == lib.PriorityType.LOW:
            priorityType = "Niski"
        elif e.prioritytype == lib.PriorityType.NORMAL:
            priorityType = "Normalny"
        elif e.prioritytype == lib.PriorityType.HIGH:
            priorityType = "Wysoki"
        table_data.append([e.event_id, e.name, e.description, event_occurrence.exactdate, periodType, priorityType])

    table = AsciiTable(table_data)
    print(table.table)
    print("\n")


def print_event_reminders(event_occurrences, reminder_date):
    table_data = [['Nazwa', 'Opis', 'Data']]
    for event_occurrence in event_occurrences:
        if reminder_date == event_occurrence.exactdate:
            e = event_occurrence.parent
            table_data.append([e.name, e.description, event_occurrence.exactdate])
    table = AsciiTable(table_data)
    print(table.table)
    print("\n")


def print_events(events):
    table_data = [['ID', 'Nazwa', 'Opis', 'Data', 'Powtarzalnosc', 'Priorytet']]
    for e in events:
        if e.periodtype == lib.PeriodType.DAILY:
            periodType = "Dzienna"
        elif e.periodtype == lib.PeriodType.WEEKLY:
            periodType = "Tygodniowa"
        elif e.periodtype == lib.PeriodType.MONTHLY:
            periodType = "Miesieczna"
        elif e.periodtype == lib.PeriodType.YEARLY:
            periodType = "Roczna"
        if e.prioritytype == lib.PriorityType.LOW:
            priorityType = "Niski"
        elif e.prioritytype == lib.PriorityType.NORMAL:
            priorityType = "Normalny"
        elif e.prioritytype == lib.PriorityType.HIGH:
            priorityType = "Wysoki"
        table_data.append([e.event_id, e.name, e.description, e.startdate, periodType, priorityType])
    table = AsciiTable(table_data)
    print(table.table)
    print("\n")