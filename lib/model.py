from enum import Enum


class PeriodType(Enum):
    DAILY = 1
    WEEKLY = 2
    MONTHLY = 3
    YEARLY = 4


class PriorityType(Enum):
    LOW = 1
    NORMAL = 2
    HIGH = 3


class Event:
    def __init__(self, event_id, name, description, startdate, periodtype, prioritytype, periodfrequency, isfinished):
        self.event_id = event_id
        self.name = name
        self.description = description
        self.startdate = startdate
        self.periodtype = periodtype
        self.prioritytype = prioritytype
        self.periodfrequency = periodfrequency
        self.isfinished = isfinished


class EventOccurrence:
    def __init__(self, parent, exactdate):
        self.parent = parent
        self.exactdate = exactdate
