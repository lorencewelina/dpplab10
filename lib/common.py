import lib
from datetime import *


class EventExpander:
    def __init__(self, event):
        self.event = event
        self.currentDate = event.startdate
        self.hasNext = True

    def next(self):
        if not self.hasNext:
            return None

        current_date_copy = self.currentDate
        if self.event.periodfrequency == 0:
            self.hasNext = False
        elif self.event.periodtype == lib.PeriodType.DAILY:
            self.currentDate = self.currentDate + timedelta(days=self.event.periodfrequency)
        elif self.event.periodtype == lib.PeriodType.WEEKLY:
            self.currentDate = self.currentDate + timedelta(weeks=self.event.periodfrequency)
        elif self.event.periodtype == lib.PeriodType.MONTHLY:
            self.currentDate = self.currentDate + timedelta(days=30*self.event.periodfrequency)
        elif self.event.periodtype == lib.PeriodType.YEARLY:
            self.currentDate = self.currentDate + timedelta(days=365*self.event.periodfrequency)

        return lib.EventOccurrence(self.event, current_date_copy)


def expand_events_to_date(limit, events):
    event_occurrences = []
    for event in events:
        eventExpander = EventExpander(event)
        event_occurrence = eventExpander.next()
        while event_occurrence is not None and event_occurrence.exactdate < limit:
            event_occurrences.append(event_occurrence)
            event_occurrence = eventExpander.next()
    return event_occurrences
