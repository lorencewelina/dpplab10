import sqlite3 as lite
import lib
import datetime



SCRIPT = """CREATE TABLE IF NOT EXISTS `Event` (
                `id` INTEGER PRIMARY KEY AUTOINCREMENT,
                `name` TEXT,
                `description` TEXT,
                `startDate` TEXT,
                `periodType` TEXT,
                `periodFrequency` INTEGER,
                `isFinished` INTEGER,
                `priority` TEXT
            );"""

DBFILE = "eventdatabase.db"


def init_db():
    with lite.connect(DBFILE) as connection:
        connection.execute(SCRIPT)


def addEvent(event):
    with lite.connect(DBFILE) as connection:
        ADDEVENT = """INSERT INTO Event (name, description, startDate, periodType, periodFrequency, isFinished, priority) VALUES (?,?,?,?,?,?,?)"""
        connection.execute(ADDEVENT, (event.name, event.description, str(event.startdate), str(event.periodtype), event.periodfrequency, event.isfinished, str(event.prioritytype)))


def deleteEvent(event):
    with lite.connect(DBFILE) as connection:
        DELETEEVENT = "DELETE FROM Event WHERE id=?"
        connection.execute(DELETEEVENT, (event.event_id,))


def getEvents():
    with lite.connect(DBFILE) as connection:
        GETEVENT = "SELECT id, name, description, startDate, periodType, periodFrequency, isFinished, priority FROM Event"
        rows = connection.execute(GETEVENT)
        events = []
        for row in rows:
            event_id, name, description, startDate, periodType, periodFrequency, isFinished, priority = row
            start_date = datetime.datetime.strptime(startDate, "%Y-%m-%d").date()
            if periodType == "PeriodType.DAILY":
                period_type = lib.PeriodType.DAILY
            elif periodType == "PeriodType.WEEKLY":
                period_type = lib.PeriodType.WEEKLY
            elif periodType == "PeriodType.MONTHLY":
                period_type = lib.PeriodType.MONTHLY
            elif periodType == "PeriodType.YEARLY":
                period_type = lib.PeriodType.YEARLY

            if priority == "PriorityType.LOW":
                priority_type = lib.PriorityType.LOW
            elif priority == "PriorityType.NORMAL":
                priority_type = lib.PriorityType.NORMAL
            elif priority == "PriorityType.HIGH":
                priority_type = lib.PriorityType.HIGH
            events.append(lib.Event(event_id, name, description, start_date, period_type, priority_type, periodFrequency, isFinished))
        return events
